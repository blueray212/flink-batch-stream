# Flink环境搭建

## 前期准备

1. Java 1.8.x或更高版本
2. ssh 每个机器之间实现无秘钥登录

## Standalone部署

下载

```text
官网下载
```

解压

```sh
tar -zxvf flink-1.10.1-bin-scala_2.11.tgz -C /opt
```

配置环境变量

```sh
# flink
export FLINK_HOME=/opt/flink-1.10.1
export PATH=$FLINK_HOME/bin:$PATH
```

查看flink版本

```sh
flink -v
```

配置flink配置文件

```sh
cd $FLINK_HOME/conf
vim flink-conf.yaml
```

flink-conf.yaml

```yaml
# 指定jobmanager的地址（可用ip/别名）
jobmanager.rpc.address: master
# 每个jobmanager的可用内存
jobmanager.heap.size：1024m
# 每个taskmanager的可用内存
taskmanager.memory.process.size: 1728m
# 每台计算机可用的CPU核数
taskmanager.numberOfTaskSlots: 12
# 集群中的CPU总数
parallelism.default: 72
# 临时目录
io.tmp.dirs: /opt/flink-1.10.1/tmp
```

> 查看逻辑cpu的个数
>
> cat /proc/cpuinfo|grep "processor"| wc -l

slaves

```sh
# 指定taskmanager的所在节点
slave01
slave02
```

分发

```sh
scp -r -P 2222 /opt/flink-1.10.1 root@slave01:/opt
scp -P 2222 ~/.bash_profile root@slave01:~/.bash_profile
```

## FAQ

1. java.lang.Exception: unable to establish the security context

由于在flink-conf.yaml中修改了临时文件路径配置项io.tmp.dirs，它不会自动创建，需要手动创建。