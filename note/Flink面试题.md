# Flink架构

## 什么是Flink?

* 分布式处理引擎
* 对有界和无界数据进行有状态的计算
* 可以运行在所有常见的集群上，并以内容速度和任意规模计算

##  Flink整体是什么样的？

![flink-architecture](../image/flink-architecture.PNG)

Flink整体架构主要分为4个层面，Deploy物理资源层、Runtime统一执行层、API层、Libraries High-level API层。

* Deploy层

> Flink任务可以运行在Local、Cluster、Cloud中

* Runtime统一执行引擎

> 针对不同的运行环境，Flink提供了统一的作业执行引擎

* API层

> 基于Runtime层，Flink提供了两种API，一种用于流处理的DataStream API，一种用于批处理的DataSet API

* High-level API层

> 在两种API之上，Flink提供了更高级的API。面向流处理支持：CEP（复杂事件处理）、基于SQL-like的操作（基于Table的关系操作）；面向批处理支持：FlinkML（机器学习库）、Gelly（图处理）、基于SQL-like的操作（基于Table的关系操作）。 

## Runtime层总体架构是什么？

Flink运行时组件主要有Dispatcher、ResourceManager、JobManager、TaskManager。

* Dispatcher（分发器）

> 分发器的主要作用是为client的应用提交提供rest接口。当一个应用被提交执行时，分发器就会启动并将应用移交给一个JobManager。由于是REST接口，所以Dispatcher可以作为集群的一个HTTP接入点，这样就能够不受防火墙阻挡。Dispatcher也会启动一个Web UI，用来方便地展示和监控作业执行的信息。Dispatcher在架构中可能并不是必需的，这取决于应用提交运行的方式。它运行在ApplicationMaster进程中。

* JobManager（作业管理器）

> 控制一个应用程序执行的主进程，也就是说，每个应用程序都会被一个不同的JobManager所控制执行。JobManager会先接收到要执行的应用程序，这个应用程序会包括：作业图（JobGraph）、逻辑数据流图（logical dataflow graph）和打包了所有的类、库和其它资源的JAR包。JobManager会把JobGraph转换成一个物理层面的数据流图，这个图被叫做“执行图”（ExecutionGraph），包含了所有可以并发执行的任务。JobManager会向资源管理器（ResourceManager）请求执行任务必要的资源，也就是任务管理器（TaskManager）上的插槽（slot）。一旦它获取到了足够的资源，就会将执行图分发到真正运行它们的TaskManager上。而在运行过程中，JobManager会负责所有需要中央协调的操作，比如说检查点（checkpoints）的协调。它运行在ApplicationMaster进程中。

* ResourceManager（资源管理器）

> 主要负责管理任务管理器（TaskManager）的插槽（slot），TaskManger插槽是Flink中定义的处理资源单元。Flink为不同的环境和资源管理工具提供了不同资源管理器，比如YARN、Mesos、K8s，以及standalone部署。当JobManager申请插槽资源时，ResourceManager会将有空闲插槽的TaskManager分配给JobManager。如果ResourceManager没有足够的插槽来满足JobManager的请求，它还可以向资源提供平台发起会话，以提供启动TaskManager进程的容器。另外，ResourceManager还负责终止空闲的TaskManager，释放计算资源。它运行在ApplicationMaster进程中。

* TaskManager（任务管理器）

> Flink中的工作进程。通常在Flink中会有多个TaskManager运行，每一个TaskManager都包含了一定数量的插槽（slots）。插槽的数量限制了TaskManager能够执行的任务数量。启动之后，TaskManager会向资源管理器注册它的插槽；收到资源管理器的指令后，TaskManager就会将一个或者多个插槽提供给JobManager调用。JobManager就可以向插槽分配任务（tasks）来执行了。在执行过程中，一个TaskManager可以跟其它运行同一应用程序的TaskManager交换数据。

![flink-yarn](../image/flink-yarn.png)

> yarn提交有两种运行模式，一种是pre-job，还有一种是session，session模式比较简单，它是在作业提交之前就已经在yarn集群上启动了AM和TM，并且共用资源，我们这里就以说pro-job模式为例。
>
> Flink采用了经典的master-slave架构，前面运行着Dsipachter、Resourcemanager、JobManager的ApplicationMaster相当于Master，后面的Taskmanager相当于slave。
>
>   首先，当我们客户端提交一个作业的时候，Client会先将用户用DataSteam、DateSet、Table API编写的作业转换成一个可以提交JobGraph（作业图），在这中间他会看哪些task可以分成一块，然后把它们栓（chain）在一起，还会去检查各个task的输入输出类型是否匹配，还有对一些job作一些额外的优化，比如说Batch Job。
>
>   Client会向yarn等资源管理器申请一个container容器，然后让yarn在容器中AM进程拉起来，启动完成后，作业会提交给Dispatcher，Dispatcher这时就会启动一个新的JM线程。
>
>   JM得到作业图后他会根据JobGraph先分析这个作业需要的资源，生成执行图（ExecutionGraph），然后他会找RM来获取所需要的资源，RM此时会向yarn去索要资源（container）和前面的启动AM一样，这时TM就会被启起来。
>
>   之后，RM会向找到TM告诉他：“你的slot被征用了！”，TM就会哭着向JM回复：“这些slot暂时归你了”并且把对应的slot标记为“正在使用”，这时候JM就可以把task Submit上去。
>
>   TM在收到task后，会为这个task启动一个新的线程，当全部的task都启起来之后多个task就可以使用flink的shuffle模块来交换数据，整个作业就可以跑起来了。

![作业提交过程](../image/作业提交过程.png)

## Flink的资源管理是怎么样的？

![Flink作业提交](../image/Flink作业提交.png)

![flink资源管理](../image/flink资源管理.png)

> 之前我们说过，Flink中也有他自己的资源资源管理器RM，具体说的话资源管理就是RM中一个叫SlotManager的一个组件，他负责维护管理Slot状态和分配空闲的Slot资源。
>
>   JobManager需要对整个作业负责，因此首先是由它发起向RM索要slot资源的请求，而在TM启动的时候，他会主动的向RM报告自己的Slot信息，包括有多少个slot，每个slot的资源大小（社区版无）。当RM收到JM的申请时，会计算集群空闲资源是否满足需求，如果达到需求，RM会向被分配的TM发送通知“你的xxSlot被征用了，请去xxx地和JM接头！”
>
>   然后，TM收到通知后，会找到JM，告诉他“你的申请成功了，这些Slot现在暂时归你了！”；JM收到Slot后，会把Slot信息缓存在一个SlotPool中，在收到足够的Slot后，JM会把task提交到对应的Slot上，也就是提交任务执行。
>
>   当作业结束，无论是异常结束还是正常结束，TM都会向RM发送释放对应Slot的请求。

##  Flink Job的提交流程 ？

用户提交的Flink Job会被转化成一个DAG任务运行，分别是：StreamGraph、JobGraph、ExecutionGraph，Flink中JobManager与TaskManager，JobManager与Client的交互是基于Akka工具包的，是通过消息驱动。整个Flink Job的提交还包含着ActorSystem的创建，JobManager的启动，TaskManager的启动和注册。

##  **Flink所谓"三层图"结构是哪几个"图"？** 

一个Flink任务的DAG生成计算图大致经历以下三个过程：

- StreamGraph
  最接近代码所表达的逻辑层面的计算拓扑结构，按照用户代码的执行顺序向StreamExecutionEnvironment添加StreamTransformation构成流式图。
- JobGraph
  从StreamGraph生成，将可以串联合并的节点进行合并，设置节点之间的边，安排资源共享slot槽位和放置相关联的节点，上传任务所需的文件，设置检查点配置等。相当于经过部分初始化和优化处理的任务图。
- ExecutionGraph
  由JobGraph转换而来，包含了任务具体执行所需的内容，是最贴近底层实现的执行图。

## Operator Chains（算子链）是什么？

为了更高效地分布式执行，Flink会尽可能地将operator的subtask链接（chain）在一起形成task。每个task在一个线程中执行。将operators链接成task是非常有效的优化：它能减少线程之间的切换，减少消息的序列化/反序列化，减少数据在缓冲区的交换，减少了延迟的同时提高整体的吞吐量。

两个operator chain在一起的的条件：

- 上下游的并行度一致
- 下游节点的入度为1 （也就是说下游节点没有来自其他节点的输入）
- 上下游节点都在同一个 slot group 中（下面会解释 slot group）
- 下游节点的 chain 策略为 ALWAYS（可以与上下游链接，map、flatmap、filter等默认是ALWAYS）
- 上游节点的 chain 策略为 ALWAYS 或 HEAD（只能与下游链接，不能与上游链接，Source默认是HEAD）
- 两个节点间数据分区方式是 forward（参考理解数据流的分区） 用户没有禁用 chain

 ## Flink集群有哪些角色？各自有什么作用？

![flink-runtime-architecture](../image/flink-runtime-architecture.PNG)

Flink程序在运行时主要有TaskManager，JobManager，Client三种角色。 

*  JobManager 

> JobManager扮演着集群中的管理者Master的角色，它是整个集群的协调者，负责接收Flink Job，协调检查点，Failover故障恢复等，同时管理Flink集群中从节点TaskManager。

*  TaskManager 

> TaskManager是实际负责执行计算的Worker，在其上执行Flink Job的一组Task，每个TaskManager负责管理其所在节点上的资源信息，如内存、磁盘、网络，在启动的时候将资源的状态向JobManager汇报。

* Client

> Client是Flink程序提交的客户端，当用户提交一个Flink程序时，会首先创建一个Client，该Client首先会对用户提交的Flink程序进行预处理，并提交到Flink集群中处理，所以Client需要从用户提交的Flink程序配置中获取JobManager的地址，并建立到JobManager的连接，将Flink Job提交给JobManager。

## Flink资源管理中Task Slot的概念是什么？

![task-solts-and-resources](../image/task-solts-and-resources.PNG)

在Flink架构角色中我们提到，TaskManager是实际负责执行计算的Worker，TaskManager 是一个 JVM 进程，并会以独立的线程来执行一个task或多个subtask。为了控制一个 TaskManager 能接受多少个 task，Flink 提出了 Task Slot 的概念。

简单的说，TaskManager会将自己节点上管理的资源分为不同的Slot：固定大小的资源子集。
 这样就避免了不同Job的Task互相竞争内存资源，但是需要主要的是，Slot只会做内存的隔离。没有做CPU的隔离。

# Flink时间语义

## Flink中的时间有哪几类？

![flink-time-1.10](../image/flink-time-1.10.PNG)

 Flink 中的时间和其他流式计算系统的时间一样分为三类：事件时间，摄入时间，处理时间三种。 

*  Event time

> 以 EventTime 为基准来定义时间窗口将形成EventTimeWindow,要求消息本身就应该携带EventTime。 

*  Ingeston time

> 以 IngesingtTime 为基准来定义时间窗口将形成 IngestingTimeWindow,以 source 的systemTime为准。 

*  Processing time

> 以 ProcessingTime 基准来定义时间窗口将形成 ProcessingTimeWindow，以 operator 的systemTime 为准。 

##  Flink中Watermark是什么概念，起到什么作用？

Watermark 是 Apache Flink 为了处理 EventTime 窗口计算提出的一种机制, 本质上是一种时间戳。
一般来讲Watermark经常和Window一起被用来处理乱序事件。 

## Flink的窗口是什么，有几种类型？

Flink支持两种划分窗口的方式，按照time和count。如果根据时间划分窗口，那么它就是一个time-window。如果根据数据划分窗口，那么它就是一个count-window。 

Flink支持窗口的两个重要属性：size和interval。

> 如果size=interval,那么就会形成tumbling-window(无重叠数据)，滚动窗口。
> 如果size>interval,那么就会形成sliding-window(有重叠数据)，滑动窗口。
> 如果size<interval, 那么这种窗口将会丢失数据。比如每5秒钟，统计过去3秒的通过路口汽车的数据，将会漏掉2秒钟的数据。

 通过组合可以得出四种基本窗口：

* time-tumbling-window 无重叠数据的时间窗口，设置方式举例：timeWindow(Time.seconds(5))
* time-sliding-window 有重叠数据的时间窗口，设置方式举例：timeWindow(Time.seconds(5), Time.seconds(3))
* count-tumbling-window无重叠数据的数量窗口，设置方式举例：countWindow(5)
* count-sliding-window 有重叠数据的数量窗口，设置方式举例：countWindow(5,3)

Flink中有四种窗口，Tumbling Windows、Sliding Windows、Session Windows、Global Windows。

# Flink容错机制

##  Flink是如何做容错的？ 

Flink实现容错主要靠强大的CheckPoint机制和State机制。Checkpoint 负责定时制作分布式快照、对程序中的状态进行备份；State 用来存储计算过程中的中间状态。

Flink的容错机制，提供了应用失败的时候重新恢复任务。这个机制主要是通过持续产生分布式快照的方式来实现的。

Flink快照主要包括两部分数据一部分是数据流的数据，另一部分是operator的状态数据。对应的快照机制的实现有主要两个部分组成，一个是屏障(Barrier），一个是状态(State)。

Flink的容错机制主要使用分布式快照实现（snapshot）。主要有两个组成，一个是屏障(Barrier），一个是状态(State)。

## Flink分布式快照的原理是什么？ 

Flink的分布式快照是根据Chandy-Lamport算法量身定做的。简单来说就是持续创建分布式数据流及其状态的一致快照。

核心思想是在 input source 端插入 barrier，控制 barrier 的同步来实现 snapshot 的备份和 exactly-once 语义。 

## Flink 中的 State Backends是什么？有什么作用？分成哪几类？说说他们各自的优缺点？

Flink流计算中可能有各种方式来保存状态：

- 窗口操作
- 使用了KV操作的函数
- 继承了CheckpointedFunction的函数
- 当开始做checkpointing的时候，状态会被持久化到checkpoints里来规避数据丢失和状态恢复。选择的状态存储策略不同，会导致状态持久化如何和checkpoints交互。
- Flink内部提供了这些状态后端:
- MemoryStateBackend
- FsStateBackend
- RocksDBStateBackend
- 如果没有其他配置，系统将使用MemoryStateBackend。

## Flink是如何实现Exactly-once的？

Flink通过状态和两次提交协议来保证了端到端的exactly-once语义。

# Flink Layer API

![flink-layer-api](../image/flink-layer-api.PNG)

# 其它

## Flink的分布式缓存是什么？怎么使用？

> Flink提供了一个分布式缓存，类似于hadoop，可以使用户在并行函数中很方便的读取本地文件，并把它放在taskmanager节点中，防止task重复拉取。
> 此缓存的工作机制如下：程序注册一个文件或者目录(本地或者远程文件系统，例如hdfs或者s3)，通过ExecutionEnvironment注册缓存文件并为它起一个名称。
> 当程序执行，Flink自动将文件或者目录复制到所有taskmanager节点的本地文件系统，仅会执行一次。用户可以通过这个指定的名称查找文件或者目录，然后从taskmanager节点的本地文件系统访问它。

## Flink的广播变量是什么？怎么使用？

> 一句话解释，可以理解为是一个公共的共享变量，我们可以把一个dataset 数据集广播出去，然后不同的任务在节点上都能够获取到，这个数据在每个节点上只会存在一份。如果不使用broadcast，则在每个节点中的每个任务中都需要拷贝一份dataset数据集，比较浪费内存(也就是一个节点中可能会存在多份dataset数据)。
>
> 注意：
>
> 1：广播出去的变量存在于每个节点的内存中，所以这个数据集不能太大，避免发生OOM。因为广播出去的数据，会常驻内存，除非程序执行结束。
>
> 2：广播变量在初始化广播出去以后不支持修改，这样才能保证每个节点的数据都是一致的。
>
> 个人建议：如果数据集在几十兆或者百兆的时候，可以选择进行广播，如果数据集的大小上G的话，就不建议进行广播了。

## 分布式缓存和广播变量的区别是什么？

> 1.广播变量是基于内存的,是将变量分发到各个worker节点的内存上（避免多次复制，节省内存）
>
> 2.分布式缓存是基于磁盘的,将文件copy到各个节点上,当函数运行时可以在本地文件系统检索该文件（避免多次复制，提高执行效率）

## 分布式缓存和广播变量的应用场景是什么？

* 分布式缓存

> 当一份要分发到各个节点上的数据是以文件形式存在时，使用分布式缓存

* 广播变量

> 当一份要分发到各个节点上的数据是以DataSet/DataStream形式存在时，使用广播变量
