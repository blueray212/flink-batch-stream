# Flink作业提交

## Standalone

**模板**

```sh
flink run [option] <jar-file> <arguments>
```

**参数**

```test
-p,--parallelism <parallelism> : job需要指定env的并行度，这个一般都需要设置

-c,--class <classname> : 需要指定的main方法的类

-C,--classpath <url> : 向每个用户代码添加url，他是通过UrlClassLoader加载。url需要指定文件的schema如（file://）

-d,--detached : 在后台运行

-q,--sysoutLogging : 禁止logging输出作为标准输出

-s,--fromSavepoint <savepointPath> : 基于savepoint保存下来的路径，进行恢复

-sas,--shutdownOnAttachedExit : 如果是前台的方式提交，当客户端中断，集群执行的job任务也会shutdown
```

**例子**

```sh
/opt/flink/bin/flink run -p 1 -c com.test.TestLocal ./flink-streaming-report-forms-1.0-SNAPSHOT-jar-with-dependencies.jar
```

