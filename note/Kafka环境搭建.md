# Kafka安装部署

## 前期准备

1. zookeeper正常运行

## 安装

下载

```text
官网下载
```

解压

```sh
tar -zxvf kafka_2.11-2.4.1.tgz -C /opt
```

配置环境变量

```sh
# kafka
export KAFKA_HOME=/opt/kafka_2.11-2.4.1
export PATH=$KAFKA_HOME/bin:$PATH
```

修改配置文件

```sh
cd $KAFKA_HOME/config
vim server.properties
```

*server.properties*

```properties
# 相当于一个kafka，每一个kafka都要配，从0开始
broker.id=0
# 添加当前机器的ip地址
listeners=PLAINTEXT://localhost:9092
# kafka日志文件存储
log.dirs=/storm/tmp/kafka-logs
# zookeeper连接地址
zookeeper.connect=ip1:2181,ip2:2181,ip3:2181
```

> mkdir -p /storm/tmp/kafka-logs

启动

```sh
# 控制台启动
kafka-server-start.sh $KAFKA_HOME/config/server.properties
# 后台启动
kafka-server-start.sh -daemon $KAFKA_HOME/config/server.properties
```

## 测试

创建topic

```sh
kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
```

查看topic

```sh
kafka-topics.sh --list --zookeeper ip1:2181
```

运行producer

```sh
kafka-console-producer.sh --broker-list localhost:9092 --topic test
```

运行consumer

```sh
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```

描述topic

```sh
kafka-topics.sh --describe --bootstrap-server localhost:9092 --topic test
```

删除topic

```sh
kafka-topics.sh --delete --zookeeper ip1:2181 --topic test1
```
