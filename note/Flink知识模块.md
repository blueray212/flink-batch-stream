[TOC]



# Flink知识图谱

Apache Flink is a framework and distributed processing engine for stateful computations over *unbounded and bounded* data streams.

## 1. Streaming Processing Concepts（common concepts for stream processing）

### Bounded and Unbounded Data and Processing

![bounded-and-unbounded-stream](../image/bounded-and-unbounded-stream.PNG)

| type                  | explain                         |
| --------------------- | ------------------------------- |
| **Unbounded streams** | have a start but no defined end |
| **Bounded streams**   | have a defined start and end    |

### Latency and Throughput

![latency-and-throughput](../image/latency-and-throughput.PNG)

牺牲延迟增加吞吐或者牺牲吞吐降低延迟。

### Time Semantics

**Flink中的时间有哪几类？**

![flink-time-1.10](../image/flink-time-1.10.PNG)

 Flink 中的时间和其他流式计算系统的时间一样分为三类：事件时间，摄入时间，处理时间三种。 

*  Event time

> 以 EventTime 为基准来定义时间窗口将形成EventTimeWindow,要求消息本身就应该携带EventTime。 

*  Ingeston time

> 以 IngesingtTime 为基准来定义时间窗口将形成 IngestingTimeWindow,以 source 的systemTime为准。 

*  Processing time

> 以 ProcessingTime 基准来定义时间窗口将形成 ProcessingTimeWindow，以 operator 的systemTime 为准。 

**Flink中的watermarks**

Watermark是Apache Flink为了处理 EventTime 窗口计算提出的一种机制, 本质上是一种时间戳。
一般来讲Watermark经常和Window一起被用来处理乱序事件。 

```http
https://blog.csdn.net/lmalds/article/details/52704170
```

**Flink的窗口是什么，有几种类型？**

Flink支持两种划分窗口的方式，按照time和count。如果根据时间划分窗口，那么它就是一个time-window。如果根据数据划分窗口，那么它就是一个count-window。 

Flink支持窗口的两个重要属性：size和interval。

> 如果size=interval,那么就会形成tumbling-window(无重叠数据)，滚动窗口。
> 如果size>interval,那么就会形成sliding-window(有重叠数据)，滑动窗口。
> 如果size<interval, 那么这种窗口将会丢失数据。比如每5秒钟，统计过去3秒的通过路口汽车的数据，将会漏掉2秒钟的数据。

 通过组合可以得出四种基本窗口：

* time-tumbling-window 无重叠数据的时间窗口，设置方式举例：timeWindow(Time.seconds(5))
* time-sliding-window 有重叠数据的时间窗口，设置方式举例：timeWindow(Time.seconds(5), Time.seconds(3))
* count-tumbling-window无重叠数据的数量窗口，设置方式举例：countWindow(5)
* count-sliding-window 有重叠数据的数量窗口，设置方式举例：countWindow(5,3)

Flink中有四种窗口：

 *tumbling windows*  滚动窗口

*sliding windows*        滑动窗口

*session windows*       会话窗口

*global windows*         全局窗口  

* Event Time

  > 以EventTime为基准来定义时间窗口将形成EventTimeWindow,要求消息本身就应该携带EventTime。 

* Processing Time

  > 以ProcessingTime基准来定义时间窗口将形成 ProcessingTimeWindow，以operator的systemTime 为准。

* Watermarks

  > Watermark 是 Apache Flink 为了处理 EventTime 窗口计算提出的一种机制, 本质上是一种时间戳。
  > 一般来讲Watermark经常和Window一起被用来处理乱序事件。
  >
  > https://blog.csdn.net/lmalds/article/details/52704170

* Window

  > *tumbling windows*    滚动窗口
  >
  > *sliding windows*        滑动窗口
  >
  > *session windows*       会话窗口
  >
  > *global windows*         全局窗口  

* Trigger

## 2. Architecture

### Component Stack

![flink-component-stack](../image/flink-component-stack.PNG)

### Layered APIs

Flink provides three layered APIs. Each API offers a different trade-off between conciseness and expressiveness and targets different use cases. 

![flink-layer-api](../image/flink-layer-api.PNG)

### Components of a Flink Setup

![flink-runtime-architecture](../image/flink-runtime-architecture.PNG)

Flink程序在运行时主要有TaskManager、JobManager、Client三种角色。

*  JobManagers
* TaskManagers
* Client

### Flink的架构是什么？

![flink-architecture](../image/flink-architecture.PNG)

Flink程序在运行时主要有Client、JobManager、TaskManager三种角色。

* Client

  > Client并不是Flink程序运行的一部分，Client主要用来向JobManager提交程序。程序提交后，Client有 *detached mode*、 *attached mode* 两种模型。`detached mode`用来触发执行，执行后就断开连接；`attached mode`是在命令行中执行，可以实时接收进度报告。

* JobManager

  > JobManager中包含三个角色：Dispatcher、JobMaster、ResourceManager。
  >
  > Dispatcher用来提交Flink应用程序和启动WebUI。
  >
  > JobMaster用来执行JobGraph。
  >
  > ResourceManager用来管理task slots。

* TaskManager

  > 执行任务，以及缓冲和交换数据流。

### Flink的运行流程

**Standalone**

![Flink-standalone运行流程](../image/Flink-standalone运行流程.png)

1. Client通过rest接口将JobGraph提交给Dispatcher；
2. Dispatcher拿到应用后，通过JobManagerRunner将JobGraph发给JobMaster；
3. JobMaster向ResourceManager申请资源；
4. ResourceManager向TaskManager请求Slot，TaskManager返回资源信息；
5. JobMaster将JobGraph转换为ExecutionGraph，并分发给TaskManager执行；
6. TaskManager执行任务并进行数据交换。

**Yarn**

![Flink-YARN运行流程](../image/Flink-YARN运行流程.png)

1. Client将jar包和配置上传到HDFS，以便JobManager和TaskManager共享HDFS的数据；
2. Client向ResourceManager提交Job，ResouceManager接到请求后，先分配container资源，然后通知NodeManager启动ApplicationMaster；
3. ApplicationMaster会加载HDFS的配置，启动对应的JobManager，然后JobManager会分析当前的作业图，将它转化成执行图（包含了所有可以并发执行的任务），从而知道当前需要的具体资源；
4. JobManager会向ResourceManager申请资源，ResouceManager接到请求后，继续分配container资源，然后通知ApplictaionMaster启动更多的TaskManager（先分配好container资源，再启动TaskManager）。container在启动TaskManager时也会从HDFS加载数据；
5. 最后，TaskManager启动后，会向JobManager发送心跳包。JobManager向TaskManager分配任务。

**Graph**

![flink-graph](../image/flink-graph.PNG)

Flink的执行图可以分为四层：StreamGraph->JobGraph->ExecutionGraph->物理执行图。启动StreamGraph和JobGraph是在客户端完成，ExecutionGraph和"物理执行层"在Cluster端完成。

* StreamGraph：是根据用户通过Stream API编写的代码生成的最初的图。用来表示程序的拓扑结构。
* JobGraph：StreamGraph经过优化后生成了JobGraph，提交给JobManager的数据结构。主要的优化为，将多个符合条件的点chain在一起作为一个节点，这样可以减少数据在节点之间流动所需要的序列化/反序列化/传输消耗。
* ExecutionGraph：JobManager根据JobGraph生成ExecutionGraph。ExecutionGraph是JobGraph的并行化版本，是调度层最核心的数据结构。
* 物理执行层：JobManager根据ExecutionGraph对Job进行调度后，在各个TaskManager上部署Task后形成的"图"，并不是一个具体的数据结构。

### Task Execution

* Operators
* Tasks

> Settting Parallelism
>
> ​	Operator Level
>
> ​	Execution Enviroment Level
>
> ​	Client Level
>
> ​	System Level（set parallelism in flink-conf.yaml）
>
> Task Failure Recovery
>
> ​	Restart Strategies
>
> ​	Failover Strategies

* Solts and Resources

  ![task-slots-and-resources](../image/task-slots-and-resources.PNG)

## 3. State Management

Flink实现容错主要靠强大的CheckPoint机制和State机制。Checkpoint 负责定时制作分布式快照、对程序中的状态进行备份；State 用来存储计算过程中的中间状态。

### State Backends

* MemoryStateBackend
* FsStateBackend
* RocksDBStateBackend

![state-backend](../image/state-backend.PNG)

### Kinds of state in Flink

* Operator State

> List state
>
> Union List state
>
> Broadcast state

* Keyed State

> Value state
>
> List state
>
> Map state

![state-type](../image/state-type.PNG)

### Fault Tolerance

* Checkpointing

> Barriers
>
> Exactly Once & At Least Once

* Savepoints

![fault-tolerance](../image/fault-tolerance.PNG)

## 4. DataStream

### Setup Environment

* Local
* Remote

### Source

* Built-in Source

> readTextFile
>
> fromCollection
>
> etc

* Custom Source

> addSource

### Sink

* Built-in Sink

> writeAsText
>
> writeAsCsv
>
> etc

* Custom Sink

> addSink

### Transformations

* Basic Transformations

> Map
>
> Filter
>
> FlatMap

* KeyedStream Transformations

> KeyBy
>
> Aggregations
>
> Reduce

* Multistream Transformations

> Union
>
> Connect，coMap，coFlatMap
>
> Split & select

* Distribution Transformations

> Random
>
> Round-Robin
>
> Rescale
>
> Broadcast
>
> Global
>
> Custom

### Data Types

* Types
* TypeInformation

### Functions

## 5. Libraries

* CEP(Complex Event Processing)
* State Process API
* Gelly

## 6. Table API & SQL

### SQL

* DDL

> Create Table/Databases/Function
>
> Drop Table/Databases/Function
>
> Alter Table/Databases/Function

* Query

> Operators（Select/Filter/Aggregations/Join/etc...）

* SQL Client

### Table API

* Java/Scala Table API
* Python Table API

### Functions

* Built-in Functions

> Scalar Functions
>
> Aggregate Functions

* User Defined Functions

> Scalar Functions
>
> Table Functions
>
> Aggregate Functions

### Connect to External Systems

* Table Connectors
* Table Formats
* Table Schema
* Update Modes

## 7. Deployment and Operations

### Deployment Modes

* Local Cluster

> 下载Flink安装包并完成单机安装后就是Local Cluster

* Standalone Cluster

> 下载Flink安装包并完成集群部署后就是Standalone Cluster

* YARN

> 

### Command-Line Interface

* Job Submission
* Job Management

## 8. Debugging and Monitoring

## 9. Ecosystem

## 10. Use Cases



