flink-annotations模块定义了一些flink项目中需要用到的注解。Java注解是附加在代码中的一些元信息，用于一些工具在编译、运行时进行解析和使用，起到说明、配置的功能。该模块主要包括的注解类型有：
![flink-annotations](../image/flink-annotations.png)
flink annotations下包含了docs相关的三种注解：ConfigGroup，ConfigGroups和Documentation。然后还有其他5种注解：Experimental，Internal， Public，PublicEnvolving和VisableForTesting。下面分别简单地介绍下这些注解的作用。

## docs相关的三个注解

### ConfigGroup

```java
@Target({})
@Internal
public @interface ConfigGroup {
	String name();
	String keyPrefix();
}
```

这个注解的作用是指定一组配置选项的类。该组的name将被用作生成的HTML文件的文件名。

### ConfigGroups

```java
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Internal
public @interface ConfigGroups {
	ConfigGroup[] groups() default {};
}
```

这个注解是提供了一种根据key的最大前缀来把配置选项拆分为不同的组。

### Documentation

这个类主要是修改文档生成器的行为的注解集合。

```java
public final class Documentation {
    
    // 用于配置选项字段的以重写已记录的默认值
	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	@Internal
	public @interface OverrideDefault {
		String value();
	}

	 // 用于配置选项字段的注释，以便将它们包括在“公共选项”部分中。
	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	@Internal
	public @interface CommonOption {
		int POSITION_MEMORY = 10;
		int POSITION_PARALLELISM_SLOTS = 20;
		int POSITION_FAULT_TOLERANCE = 30;
		int POSITION_HIGH_AVAILABILITY = 40;
		int POSITION_SECURITY = 50;

		int position() default Integer.MAX_VALUE;
	}
   
   // 在配置选项字段上使用的注释，以从文档中排除配置选项。
	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	@Internal
	public @interface ExcludeFromDocumentation {
 
		String value() default "";
	}

	private Documentation(){
	}
}
```

## Experimental

```java
@Documented
@Target(ElementType.TYPE)
@Public
public @interface Experimental {
}
```

标注类为实验阶段。带有此注释的类既没有经过严格的测试，也还不稳定，并且可以更改或删除在未来版本中。

## Internal

```java
@Documented
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR })
@Public
public @interface Internal {
}
```

该注解用于将稳定的公共API中的方法标记为内部开发人员API。开发人员API是稳定的，但仅仅是在Flink内部，但是在发布版本有可能有些变化。

## Public

```java
@Documented
@Target(ElementType.TYPE)
@Public
public @interface Public {}
```

标注类为开放和稳定的。类，方法或者属性被这个这个注解修饰时，表示在小版本迭代中，都维持稳定。

## PublicEvolving

```java
@Documented
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Public
public @interface PublicEvolving {
}
```

该注解用来标注公共的但有不断发展的接口依赖的类或者方法。带有此注释的类和方法用于公共使用，并且具有稳定的行为。但是，它们的接口和签名不被认为是稳定的，并且当跨版本时可能会变化。

## VisibleForTesting

```java
@Documented
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Internal
public @interface VisibleForTesting {}
```

这个注解申明有些函数，属性，构造函数或整个类型值是在test时才是可见的。当例如方法应该是编码阶段，通常附加这个注释（因为它不打算在外部调用），但不能声明为私有，因为一些测试需要访问它。

## 总结

到此，flink-annotation部分就介绍完了，整体回顾一下，主要介绍了flink项目中可能用到的注解。我们在脑海中要留下一个印象。

> 参考：https://blog.csdn.net/hxcaifly/article/details/84558346