# Flink编程

## State

Flink根据数据集是否按key进行分区，将状态分为Keyed State和Operator State（Non-keyed State）两种类型

**Keyed State**

* ValueState
* MapState
* ListState

```scala
// 定义mapState
var mapState: MapState[String, Int] = _

// 初始化阶段，基于MapStateDescriptor创建MapState
override def open(parameters: Configuration): Unit = {
   val mapStateDes = new MapStateDescriptor[String, Int]("lastUtcDes", classOf[String], classOf[Int])
    mapState = getRuntimeContext.getMapState(mapStateDes)
}
```

**Operator State**



## StateBackend

* MemoryStateBackend
* FsStateBackend
* RocksDBStateBackend

```scala
// 设置FsStateBackend为状态后端
env.setStateBackend(new FsStateBackend("hdfs://namenode:40010/flink/checkpoints"));
```

## CheckPoint

* EXACTLY_ONCE
* AT_LEAST_ONCE

```scala
// Checkpoint
env.enableCheckpointing(5000, CheckpointingMode.EXACTLY_ONCE)
```

注册本地hdfs文件

```sh
注册一个本地/HDFS文件
env.registerCachedFile(filePath, "pk-java-dc");
```

## Time

* Processing time
* Event Time

```scala
env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime)

data.assignAscendingTimestamps(_.timestamp)
```

## Broadcast

```scala
val broadcast = env.().broadcast()

data.connect(broadcast)
```

## Watermark

```sh
assignTimestampsAndWatermarks(new AssignerWithPeriodicWatermarks[(Long, String, Long)] 
```

## Window

```sh
window(TumblingEventTimeWindows.of(Time.seconds(60)))
```

## Transformation

kafka sink\source

```sh
FlinkKafkaProducer\FlinkKafkaConsumer
```

自定义source

```sh
RichSourceFunction
```

计数器和广播变量

```scala
getAccumulatorResult("ele-counts-java");
```

## 定时器

```scala
// 注册定时器
ctx.timerService().registerProcessingTimeTimer(System.currentTimeMillis() + 5000)

// 定时器
override def onTimer(timestamp: Long,...): Unit = {
  // 具体业务
  // 运行结束后重新注册定时器
  ctx.timerService().registerProcessingTimeTimer(timestamp + 5000)
}
```

## 