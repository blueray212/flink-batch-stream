package com.sweetdream.profile

import java.util

import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.api.scala._

/**
 * Title: 用户画像
 * Description: 整理泰坦尼克号用户画像
 * Date 2020/12/10
 */
object PersonaProfile {
  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val resource = getClass.getResource("/train.csv")
    val data = env.readCsvFile[Person](resource.getPath, ignoreFirstLine = true,
      pojoFields = Array("PassengerId", "Survived", "Pclass", "Name", "Sex", "Age", "SibSp", "Parch", "Ticket", "Fare", "Cabin", "Embarked"))
    data.print()
  }
}
