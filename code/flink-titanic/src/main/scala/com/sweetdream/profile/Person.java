package com.sweetdream.profile;

/**
 * Title:
 * Description:
 * Date 2020/12/10
 */
public class Person {
    private int PassengerId;
    private int Survived;
    private int Pclass; // 车票等级
    private String Name;
    private String Sex;
    private String Age;
    private int SibSp; // 兄弟姐妹/配偶数
    private int Parch; // 父母/孩子数
    private String Ticket; // 车票号
    private double Fare; // 车票价格
    private String Cabin; // 机舱号
    private String Embarked; // 登船港口

    public Person() {
    }

    public int getPassengerId() {
        return PassengerId;
    }

    public void setPassengerId(int passengerId) {
        PassengerId = passengerId;
    }

    public int getSurvived() {
        return Survived;
    }

    public void setSurvived(int survived) {
        Survived = survived;
    }

    public int getPclass() {
        return Pclass;
    }

    public void setPclass(int pclass) {
        Pclass = pclass;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public int getSibSp() {
        return SibSp;
    }

    public void setSibSp(int sibSp) {
        SibSp = sibSp;
    }

    public int getParch() {
        return Parch;
    }

    public void setParch(int parch) {
        Parch = parch;
    }

    public String getTicket() {
        return Ticket;
    }

    public void setTicket(String ticket) {
        Ticket = ticket;
    }

    public double getFare() {
        return Fare;
    }

    public void setFare(double fare) {
        Fare = fare;
    }

    public String getCabin() {
        return Cabin;
    }

    public void setCabin(String cabin) {
        Cabin = cabin;
    }

    public String getEmbarked() {
        return Embarked;
    }

    public void setEmbarked(String embarked) {
        Embarked = embarked;
    }

    @Override
    public String toString() {
        return "Person{" +
                "PassengerId=" + PassengerId +
                ", Survived=" + Survived +
                ", Pclass=" + Pclass +
                ", Name='" + Name + '\'' +
                ", Sex='" + Sex + '\'' +
                ", Age=" + Age +
                ", SibSp=" + SibSp +
                ", Parch=" + Parch +
                ", Ticket='" + Ticket + '\'' +
                ", Fare=" + Fare +
                ", Cabin='" + Cabin + '\'' +
                ", Embarked='" + Embarked + '\'' +
                '}';
    }
}
