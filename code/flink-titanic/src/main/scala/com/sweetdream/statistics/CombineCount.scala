package com.sweetdream.statistics

import com.sweetdream.profile.Person
import org.apache.flink.api.scala.{ExecutionEnvironment, _}

/**
 * Title: 多个属性联合统计
 * Description:
 * Date 2020/12/10
 */
object CombineCount {
  def main(args: Array[String]): Unit = {
    // 1. env
    val env = ExecutionEnvironment.getExecutionEnvironment

    // 2. source
    val resource = getClass.getResource("/train.csv")
    val data = env.readCsvFile[Person](resource.getPath, ignoreFirstLine = true,
      pojoFields = Array("PassengerId", "Survived", "Pclass", "Name", "Sex", "Age", "SibSp", "Parch", "Ticket", "Fare", "Cabin", "Embarked"))

    // 3. transform
    // TODO...统计性别存活数，各年龄段存活数
    val sexSurvivedCount = data.map(x => ((x.getSex, x.getSurvived), 1)).groupBy(0).sum(1)

    //    val survivedCount = data.map(x => (x.getSurvived, 1)).groupBy(0).sum(1)

    // 4. sink
    sexSurvivedCount.print()
    //    survivedCount.print()
  }
}
