package com.sweetdream.gelly

import org.apache.flink.api.common.functions.MapFunction
import org.apache.flink.api.scala._
import org.apache.flink.graph.scala._
import org.apache.flink.graph.scala.utils.Tuple3ToEdgeMap
import org.apache.flink.graph.spargel.{GatherFunction, MessageIterator, ScatterFunction}
import org.apache.flink.graph.{Edge, Vertex}

import scala.collection.JavaConversions._

/**
 * Title: 
 * Description: 
 * Date 2020/12/16
 */
object SingleSourceShortestPaths {
  def main(args: Array[String]) {
    if (!parseParameters(args)) {
      return
    }

    // 1.env
    val env = ExecutionEnvironment.getExecutionEnvironment

    // 2.source
    val edges: DataSet[Edge[Long, Double]] = getEdgesDataSet(env)

    // 3.transformation
    val graph = Graph.fromDataSet[Long, Double, Double](edges, new InitVertices(srcVertexId), env)

    // Execute the scatter-gather iteration
    val result = graph.runScatterGatherIteration(new MinDistanceMessenger,
      new VertexDistanceUpdater, maxIterations)

    // Extract the vertices as the result
    val singleSourceShortestPaths = result.getVertices

    // 4.sink
    if (fileOutput) {
      singleSourceShortestPaths.writeAsCsv(outputPath, "\n", ",")
      env.execute("Single Source Shortest Paths Example")
    } else {
      singleSourceShortestPaths.print()
    }
  }

  // --------------------------------------------------------------------------------------------
  //  Single Source Shortest Path UDFs
  // --------------------------------------------------------------------------------------------

  private final class InitVertices(srcId: Long) extends MapFunction[Long, Double] {

    override def map(id: Long) = {
      if (id.equals(srcId)) {
        0.0
      } else {
        Double.PositiveInfinity
      }
    }
  }

  /**
   * Distributes the minimum distance associated with a given vertex among all
   * the target vertices summed up with the edge's value.
   */
  private final class MinDistanceMessenger extends
    ScatterFunction[Long, Double, Double, Double] {

    override def sendMessages(vertex: Vertex[Long, Double]) {
      if (vertex.getValue < Double.PositiveInfinity) {
        for (edge: Edge[Long, Double] <- getEdges) {
          sendMessageTo(edge.getTarget, vertex.getValue + edge.getValue)
        }
      }
    }
  }

  /**
   * Function that updates the value of a vertex by picking the minimum
   * distance from all incoming messages.
   */
  private final class VertexDistanceUpdater extends GatherFunction[Long, Double, Double] {

    override def updateVertex(vertex: Vertex[Long, Double], inMessages: MessageIterator[Double]) {
      var minDistance = Double.MaxValue
      while (inMessages.hasNext) {
        val msg = inMessages.next
        if (msg < minDistance) {
          minDistance = msg
        }
      }
      if (vertex.getValue > minDistance) {
        setNewVertexValue(minDistance)
      }
    }
  }

  // ****************************************************************************
  // UTIL METHODS
  // ****************************************************************************

  private var fileOutput = false
  private var srcVertexId = 1L
  private var edgesInputPath: String = null
  private var outputPath: String = null
  private var maxIterations = 5

  private def parseParameters(args: Array[String]): Boolean = {
    if (args.length > 0) {
      if (args.length != 4) {
        System.err.println("Usage: SingleSourceShortestPaths <source vertex id>" +
          " <input edges path> <output path> <num iterations>")
      }
      fileOutput = true
      srcVertexId = args(0).toLong
      edgesInputPath = args(1)
      outputPath = args(2)
      maxIterations = 3
    } else {
      System.out.println("Executing Single Source Shortest Paths example "
        + "with default parameters and built-in default data.")
      System.out.println("  Provide parameters to read input data from files.")
      System.out.println("  See the documentation for the correct format of input files.")
      System.out.println("Usage: SingleSourceShortestPaths <source vertex id>" +
        " <input edges path> <output path> <num iterations>")
    }
    true
  }

  private def getEdgesDataSet(env: ExecutionEnvironment): DataSet[Edge[Long, Double]] = {
    if (fileOutput) {
      env.readCsvFile[(Long, Long, Double)](edgesInputPath,
        lineDelimiter = "\n",
        fieldDelimiter = "\t")
        .map(new Tuple3ToEdgeMap[Long, Double]())
    } else {
      val edgeData = SingleSourceShortestPathsData.DEFAULT_EDGES map {
        case Array(x, y, z) => (x.asInstanceOf[Long], y.asInstanceOf[Long],
          z.asInstanceOf[Double])
      }
      env.fromCollection(edgeData).map(new Tuple3ToEdgeMap[Long, Double]())
    }
  }
}
