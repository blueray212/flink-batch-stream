package com.sweetdream.ml.linearregression

import com.sweetdream.ml.LinearRegressionData
import org.apache.flink.api.common.functions._
import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.api.scala._
import org.apache.flink.configuration.Configuration

import scala.collection.JavaConverters._

/**
 * Title: 线性回归
 * Description:
 * Date 2020/12/16
 */
object LinearRegression {
  def main(args: Array[String]) {
    // 1.env
    val env = ExecutionEnvironment.getExecutionEnvironment

    val params: ParameterTool = ParameterTool.fromArgs(args)
    // make parameters available in the web interface
    env.getConfig.setGlobalJobParameters(params)

    // 2.source
    val parameters = env.fromCollection(LinearRegressionData.PARAMS map {
      case Array(x, y) => Params(x.asInstanceOf[Double], y.asInstanceOf[Double])
    })

    val data =
      if (params.has("input")) {
        env.readCsvFile[(Double, Double)](
          params.get("input"),
          fieldDelimiter = " ",
          includedFields = Array(0, 1))
          .map { t => new Data(t._1, t._2) }
      } else {
        println("Executing com.sweetdream.ml.LinearRegression example with default input data set.")
        println("Use --input to specify file input.")
        val data = LinearRegressionData.DATA map {
          case Array(x, y) => Data(x.asInstanceOf[Double], y.asInstanceOf[Double])
        }
        env.fromCollection(data)
      }

    // 3.transformation
    val numIterations = params.getInt("iterations", 10)

    val result = parameters.iterate(numIterations) { currentParameters =>
      val newParameters = data
        .map(new SubUpdate).withBroadcastSet(currentParameters, "parameters")
        .reduce { (p1, p2) =>
          val result = p1._1 + p2._1
          (result, p1._2 + p2._2)
        }
        .map { x => x._1.div(x._2) }
      newParameters
    }

    // 4.sink
    if (params.has("output")) {
      result.writeAsText(params.get("output"))
      env.execute("Scala Linear Regression example")
    } else {
      println("Printing result to stdout. Use --output to specify output path.")
      result.print()
    }
  }

  /**
   * A simple data sample, x means the input, and y means the target.
   */
  case class Data(var x: Double, var y: Double)

  /**
   * A set of parameters -- theta0, theta1.
   */
  case class Params(theta0: Double, theta1: Double) {
    def div(a: Int): Params = {
      Params(theta0 / a, theta1 / a)
    }

    def +(other: Params) = {
      Params(theta0 + other.theta0, theta1 + other.theta1)
    }
  }

  // *************************************************************************
  //     USER FUNCTIONS
  // *************************************************************************

  /**
   * Compute a single BGD type update for every parameters.
   */
  class SubUpdate extends RichMapFunction[Data, (Params, Int)] {

    private var parameter: Params = null

    /** Reads the parameters from a broadcast variable into a collection. */
    override def open(parameters: Configuration) {
      val parameters = getRuntimeContext.getBroadcastVariable[Params]("parameters").asScala
      parameter = parameters.head
    }

    def map(in: Data): (Params, Int) = {
      val theta0 =
        parameter.theta0 - 0.01 * ((parameter.theta0 + (parameter.theta1 * in.x)) - in.y)
      val theta1 =
        parameter.theta1 - 0.01 * (((parameter.theta0 + (parameter.theta1 * in.x)) - in.y) * in.x)
      (Params(theta0, theta1), 1)
    }
  }

}
