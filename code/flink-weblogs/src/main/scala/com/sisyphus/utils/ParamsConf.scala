package com.sisyphus.utils

import com.typesafe.config.ConfigFactory

/**
 * 配置管理
 */
object ParamsConf {
  private lazy val config = ConfigFactory.load()

  // kafka
  val kafkaTopic = config.getString("kafka.topic")
  val kafkaHosts = config.getString("kafka.hosts")
  val kafkaGroupId = config.getString("kafka.group.id")

  // postgresql
  val postgresqlDriver = config.getString("postgresql.driver")
  val postgresqlUrl = config.getString("postgresql.url")
  val postgresqlUser = config.getString("postgresql.user")
  val postgresqlPassword = config.getString("postgresql.password")

  def main(args: Array[String]): Unit = {
    println(kafkaHosts)
  }
}
