package com.sisyphus.source

import java.sql.{Connection, DriverManager, PreparedStatement}
import java.util.concurrent.TimeUnit

import com.sisyphus.utils.ParamsConf
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.functions.source.{RichSourceFunction, SourceFunction}

import scala.collection.mutable

/**
 * 读取mysql中用户域名表
 */
class MySQLSource extends RichSourceFunction[mutable.HashMap[String, String]] {

  var connection: Connection = null
  var ps: PreparedStatement = null

  /**
   * open: 建立连接
   */
  override def open(parameters: Configuration): Unit = {

    //    val driver = "com.mysql.jdbc.Driver"
    //    val url = "jdbc:mysql://192.168.199.233:3306/flink"
    //    val user = "root"
    //    val password = "root"
    //    Class.forName(driver)

    val driver = ParamsConf.postgresqlDriver
    val url = ParamsConf.postgresqlUrl
    val user = ParamsConf.postgresqlUser
    val password = ParamsConf.postgresqlPassword
    Class.forName(driver)

    connection = DriverManager.getConnection(url, user, password)

    val sql = "select user_id,domain from user_domain_config"
    ps = connection.prepareStatement(sql)
  }

  /**
   * 释放资源
   */
  override def close(): Unit = {
    if (ps != null) {
      ps.close()
    }

    if (connection != null) {
      connection.close()
    }
  }

  /**
   * 从MySQL表中把数据读取出来转成Map
   */
  override def run(sourceContext: SourceFunction.SourceContext[mutable.HashMap[String, String]]): Unit = {
    for (i <- 1 to 10) {
      val map = mutable.HashMap[String, String]()

      val resultSet = ps.executeQuery()
      while (resultSet.next()) {
        // domain,userId
        map.put(resultSet.getString(2), resultSet.getString(1))
        // 发送结果
        sourceContext.collect(map)
      }
      TimeUnit.MINUTES.sleep(1)
    }
  }

  override def cancel(): Unit = {
  }
}
