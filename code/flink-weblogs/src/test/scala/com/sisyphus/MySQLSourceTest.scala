package com.sisyphus

import com.sisyphus.source.MySQLSource
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.api.scala._

object MySQLSourceTest {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment
    env.setParallelism(1)

    val data = env.addSource(new MySQLSource)
    data.print()

    data.map(x => {
      x.get("v4.go2yd.com").getOrElse("0")
    })
      .print()

    env.execute("MySQLSourceTest")
  }
}
