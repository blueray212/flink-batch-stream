package com.sisyphus

import java.util.Properties

import org.apache.flink.api.common.functions.RichMapFunction
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.flink.streaming.util.serialization.SimpleStringSchema


object StreamingTest {
  def main(args: Array[String]): Unit = {
    // 1. enviroment
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // 引入隐式转换
    import org.apache.flink.api.scala._

    val properties = new Properties()
    properties.setProperty("bootstrap.servers", "172.22.11.106:9092")
    properties.setProperty("group.id", "log-group")
    // 2. source
    val data = env.addSource(new FlinkKafkaConsumer[String]("weblogs", new SimpleStringSchema(), properties))
      .map(new RichMapFunction[String, String] {
        override def open(parameters: Configuration): Unit = {
          println("map")
        }

        override def map(value: String): String = value
      })
      .print()

    // 5. execute
    env.execute("StreamingWCScalaApp")
  }
}
