# 网站日志分析项目实战

## 项目架构

![Flink项目实战架构](../../image/Flink项目实战架构.PNG)

<center>图1 网站日志分析架构图</center>

1. Data：`Kakfa`模拟生成实时数据
2. Source：`FlinkKafkaConsumer`接收数据，自定义`mysql source`读取数据库数据
3. Transformation：`Flink`对数据进行统计分析
4. Sink：将统计结果数据到`ES`中

## 业务描述

1. 统计一分钟内每个域名访问产生的流量
2. 统计一分钟内每个用户产生的流量

## 业务概述

### 计算概述

> 业务1

![weblog-1](../../image/weblog-1.svg)

<center>图2 每分钟每个域名访问流量计算流程</center>

> 业务2

![weblog-2](../../image/weblog-2.svg)

<center>图3 每分钟每个用户访问流量计算流程</center>

### 输入数据

> 业务1

<center>表1 用户访问日志</center>

| name  | country | level |        time         |       ip       |    domain    | traffic |
| :---: | :-----: | :---: | :-----------------: | :------------: | :----------: | :-----: |
| imooc |   CN    |   E   | 2020-07-27 20:51:13 | 183.225.139.16 | v2.go2yd.com |  7405   |

> 业务2

<center>表2 用户访问日志</center>

| name  | country | level |        time         |       ip       |    domain    | traffic |
| :---: | :-----: | :---: | :-----------------: | :------------: | :----------: | :-----: |
| imooc |   CN    |   E   | 2020-07-27 20:51:13 | 183.225.139.16 | v2.go2yd.com |  7405   |

<center>表3 用户域名表</center>

| userId  |      domain      |
| :-----: | :--------------: |
| 8000000 |   v1.go2yd.com   |
| 8000000 |   v2.go2yd.com   |
| 8000000 |   v3.go2yd.com   |
| 8000000 |   v4.go2yd.com   |
| 8000001 | test.gifshow.com |

### 输出数据

> 业务1

<center>表4 每个域名一分钟内流量总和</center>

|       time       |    domain    | traffic |
| :--------------: | :----------: | :-----: |
| 2019-09-2020：20 | v2.go2yd.com |  23000  |

> 业务2

<center>表5 每个用户一分钟内流量总和</center>

|       time       | userId  | traffic |
| :--------------: | :-----: | :-----: |
| 2019-09-2020：20 | 8000000 |  23000  |

## 详细说明

### 计算过程

> 业务1

1. 接收`kafka`中的网站日志数据
2. 清洗数据，选择level=E的domain、traffic、time
3. 统计每个域名一分钟内的流量总和
4. 打印输出

> 业务2

1. 接收`kafka`中的网站日志数据
2. 清洗数据，选择level=E的domain、traffic、time
3. 读取`mysql`中的“用户域名表”
4. 将清洗后的数据与读取`mysql`的数据连接，提取userId、traffic、time
5. 统计每个用户一分钟内的流量总和
6. 打印输出

## 附录

测试消费数据，`kafka`启动命令

```sh
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
```

