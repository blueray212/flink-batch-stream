package com.sisyphus.wordcount

import org.apache.flink.api.scala.ExecutionEnvironment

/**
 * Title: 批处理
 * Description:
 * Author sweetdream
 * Date 2020/12/11
 */
object Batch {
  def main(args: Array[String]): Unit = {
    // 1. enviroment
    val env = ExecutionEnvironment.getExecutionEnvironment

    // 2. source
    val path = this.getClass.getClassLoader.getResource("wordcount").getPath
    val data = env.readTextFile(path)

    // 引入隐式转换
    import org.apache.flink.api.scala._

    // 3. transformation
    val res = data
      .flatMap(_.toLowerCase.split(" "))
      .filter(_.nonEmpty)
      .map((_, 1))
      .groupBy(0)
      .sum(1)

    // 4. sink
    res.print()
  }
}
