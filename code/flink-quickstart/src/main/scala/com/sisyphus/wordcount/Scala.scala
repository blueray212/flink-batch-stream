package com.sisyphus.wordcount

/**
 * Title: Scala
 * Description: 原生api实现
 * Author sweetdream
 * Date 2020/12/11
 */
object Scala {
  def main(args: Array[String]): Unit = {
    val array = Array("good good study", "day day up")

    array
      .flatMap(_.split(" "))
      .map((_, 1))
      .groupBy(_._1)
      .mapValues(_.foldLeft(0)(_ + _._2))
      .foreach(println(_))
  }
}
