package com.sisyphus.wordcount

import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.time.Time

/**
 * Title: 流处理
 * Description: 使用readTextFile
 * Author sweetdream
 * Date 2020/12/11
 */
object StreamReadTextFile {
  def main(args: Array[String]): Unit = {
    // 1. enviroment
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    // 引入隐式转换
    import org.apache.flink.api.scala._

    // 2. source
    val data = env.readTextFile(getClass.getResource("/wordcount").getPath)

    // 3. transformation
    val res = data.flatMap(_.split(" "))
      .map((_, 1))
      .keyBy(0)
      .sum(1)

    // 4. sink
    res.print().setParallelism(1)

    // 5. execute
    env.execute("stream readtextfile")
  }
}
