# Flink内容总结

## Flink基础

* [Quick Start：分别使用Scala、DataSet、DataStream、Table实现了WordCount](code/flink-quickstart)
* [Flink环境搭建](note/Flink环境搭建.md)
* [Flink入门](note/Flink入门.md)
* [Kafka环境搭建](note/Kafka环境搭建.md)

## Flink组件

* [Flink Explore：对Flink的各组件的API进行探索，包括DataSet、DataStream、Table & SQL、CEP、Gelly、ML](code/flink-explore)

* [Flink编程](note/Flink编程.md)

## Flink实战

* [Flink项目实战1：网站日志流量统计分析](code/flink-weblogs)
* [Flink项目实战2：电商用户行为分析](code/UserBehaviorAnalysis)
* [Flink作业提交、优化和监控](note/Flink作业提交.md)

## Flink面试

* [Flink知识模块](note/Flink知识模块.md)
* [Flink面试题](note/Flink面试题.md)

## Flink经验

* [Flink在项目中的遇到的问题](note/Flink项目经验.md)

## Flink源码

* [Flink执行流程](note/Flink执行流程.md)
* [Flink工程目录](note/Flink工程目录.md)
* [flink-annotations](note/flink-annotations.md)